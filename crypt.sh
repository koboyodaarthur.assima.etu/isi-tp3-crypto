#!/bin/bash

openssl enc -aes-256-ecb -in ramdisk/database -out ramdisk/databasetmp.crypt -K $(cat usb0/password)
openssl enc -aes-256-ecb -in ramdisk/databasetmp.crypt -out dd/database.crypt -K $(cat usb1/password)
rm ramdisk/databasetmp.crypt
rm ramdisk/database

