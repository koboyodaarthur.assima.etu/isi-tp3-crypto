echo "Dechiffrage"
openssl enc -d -aes-256-ecb -in dd/database.crypt -out ramdisk/databasetmp -K $(cat usb1/password)
openssl enc -d -aes-256-ecb -in ramdisk/databasetmp -out ramdisk/database -K $(cat usb0/password)

rm ramdisk/databasetmp

