echo "Dechiffrage"
openssl enc -d -aes-256-ecb -in dd/database.crypt -out ramdisk/databasetmp -K $(cat $1/password)
openssl enc -d -aes-256-ecb -in ramdisk/databasetmp -out ramdisk/database -K $(cat $2/password)

rm ramdisk/databasetmp

