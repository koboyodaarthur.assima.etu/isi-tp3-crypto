Arthur Assima
Nordine El Ammari

# Question 1

Chaque responsable doit dans un premier s'identifier pour accéder au contenu de sa clé USB. Une fois le contenu atteint on effectue un XOR des deux clés de cryptage présentent sur les clés USB pour déchiffrer le contenu du fichier sur le serveur.

# Question 2

### initialiser les disk et les USB de cette manière:

- ramdisk
- dd
- usb0
- usb1
- dd/database (contiendra les paires nom : carte bancaire)

```
dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > usb0/password
dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > usb1/password
gpg -c usb0/password
gpg -c usb1/password
```

### mise en service

```
gpg usb0/password.gpg
gpg usb1/password.gpg
```

### ajouter une paire

```
bash add.sh $nom $numero
```

### supprimer une paire

```
bash delete.sh $nom $numero
```

### chercher les numero de cartes associer à un nom

```
bash search.sh $nom
```
### cryptage

```
bash crypt.sh
```
supprimer password dans les dossiers usb0 et usb1
recharger le service :

```
echo RELOADAGENT | gpg-connect-agent
```

# Question 3

Les responsables et les représentants partagerons la même clé mais chacun aura une clé usb differente avec un mot de passe lui aussi different
