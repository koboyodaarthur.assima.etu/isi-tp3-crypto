#!/bin/bash

openssl enc -aes-256-ecb -in ramdisk/database -out ramdisk/databasetmp.crypt -K $(cat $1/password)
openssl enc -aes-256-ecb -in ramdisk/databasetmp.crypt -out dd/database.crypt -K $(cat $2/password)
rm ramdisk/databasetmp.crypt
rm ramdisk/database

